package com.demo.userweb.management.mbean;

import com.demo.userweb.web.domain.User;

public interface UserManagerMBean {
	
	public Long getId();
	public void setId(Long id);
	public String getName();
	public void setName(String name);
	public String getPassword();
	public void setPassword(String password);
	public User getUser();
}
