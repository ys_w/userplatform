package com.demo.userweb.management.dynamicbean;

import com.demo.userweb.web.domain.User;

import javax.management.*;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class DynamicSampleBean implements DynamicMBean {

    /**
     *  Managed Resource
     */
    private Map<String, String> beanValues  = new HashMap<>();

    public DynamicSampleBean(){
        beanValues.put("id", "5");
        beanValues.put("name", "sample");
        beanValues.put("password", "password");
    }

    @Override
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException {
        return beanValues.get(attribute);
    }

    @Override
    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        Object attributeValue = attribute.getValue();
        if (attributeValue instanceof String){
            beanValues.putIfAbsent(attribute.getName(), String.class.cast(attributeValue));
        }
    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        AttributeList attributeList = new AttributeList();
        for (String key : beanValues.keySet()){
            attributeList.add(new Attribute(key, beanValues.get(key)));
        }
        return attributeList;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        AttributeList attributesRetrived = new AttributeList();
        if (attributes != null){
            for (Attribute attribute : attributes.asList()){
                String name = attribute.getName();
                Object value = attribute.getValue();
                if ( !(value instanceof String) ){
                    continue;
                }
                beanValues.put(name, String.class.cast(value));
                attributesRetrived.add(new Attribute(name, value));
            }
        }
        return attributesRetrived;
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
        int paramsLen = params == null ? 0 : params.length;
        int signatureLen = signature == null ? 0 : signature.length;

        if ( ("get".equals(actionName)) && (paramsLen == 1) && (signatureLen == 1) ){
            return beanValues.get(params[0]);
        }
        return null;
    }

    /**
     * 3 attributes
     * 1 operation
     */
    @Override
    public MBeanInfo getMBeanInfo() {
        MBeanAttributeInfo[] mBeanAttributeInfo = {
            new MBeanAttributeInfo("id", "java.lang.String", "id", true, true, false),
            new MBeanAttributeInfo("name", "java.lang.String", "name", true, true, false),
            new MBeanAttributeInfo("password", "java.lang.String", "password", true, true, false)
        };

        Constructor[] constructors = User.class.getConstructors();
        MBeanConstructorInfo[] mBeanConstructorInfo = null;
        if (constructors != null){
            mBeanConstructorInfo = new MBeanConstructorInfo[constructors.length];
            int index = 0;
            for (Constructor constructor : constructors){
                mBeanConstructorInfo[index] = new MBeanConstructorInfo("user constructor", constructor);
                index ++;
            }
        }

        MBeanParameterInfo[] params = { new MBeanParameterInfo("key", "java.lang.String", "key for retrieve value from map") };

        MBeanOperationInfo[] mBeanOperationInfo = {
                new MBeanOperationInfo("get", "getter", params, "java.lang.String", MBeanOperationInfo.INFO)
        };

        return new MBeanInfo(this.getClass().getSimpleName(), "MBean for User", mBeanAttributeInfo, mBeanConstructorInfo, mBeanOperationInfo, null);
    }

}
