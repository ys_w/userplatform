package com.demo.userweb.management.mxbean;

import java.util.Date;
import java.util.Queue;

/**
 *   MXBean
 *   simple types such as int or String to remain unchanged,
 *   while complex types such as MemoryUsage get mapped to the standard type CompositeDataSupport
 */
public class QueueSampler implements QueueSamplerMXBean{

    private Queue<String> queue;

    public QueueSampler(Queue<String> queue){
        this.queue = queue;
    }

    @Override
    public QueueSample getQueueSample() {
        synchronized (queue){
            return new QueueSample(new Date(), queue.size(), queue.peek());
        }
    }

    @Override
    public void clearQueue() {
        synchronized (queue){
            queue.clear();
        }
    }

}
