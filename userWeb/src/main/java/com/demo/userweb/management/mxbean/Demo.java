package com.demo.userweb.management.mxbean;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Demo {
    public static void main(String[] args) throws Exception{
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        ObjectName mXBeanName = new ObjectName("com.demo.userweb.management.mxbean:type=QueueSampler");

        Queue<String> queue = new ArrayBlockingQueue<String>(10);
        queue.add("Request-1");
        queue.add("Request-2");
        queue.add("Request-3");
        QueueSampler queueSampler = new QueueSampler(queue);

        server.registerMBean(queueSampler, mXBeanName);

        Thread.sleep(Long.MAX_VALUE);
    }
}
