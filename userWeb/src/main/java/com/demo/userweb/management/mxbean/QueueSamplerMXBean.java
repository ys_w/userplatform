package com.demo.userweb.management.mxbean;

public interface QueueSamplerMXBean {
    public QueueSample getQueueSample();
    public void clearQueue();
}

