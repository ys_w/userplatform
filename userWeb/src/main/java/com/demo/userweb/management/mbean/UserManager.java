package com.demo.userweb.management.mbean;

import com.demo.userweb.web.domain.User;

/*
	Standard MBean
 */
public class UserManager implements UserManagerMBean{

	private User user;
	
	public  UserManager(User user) {
		this.user = user;
	}
	
	@Override
	public Long getId() {
		return user.getId();
	}

	@Override
	public void setId(Long id) {
		user.setId(id);
	}

	@Override
	public String getName() {
		return user.getName();
	}

	@Override
	public void setName(String name) {
		user.setName(name);
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public void setPassword(String password) {
		user.setPassword(password);
	}
	
	@Override
	public String toString() {
		System.out.println(user.toString());
		return user.toString();
	}

	@Override
	public User getUser() {
		return user;
	}
}
