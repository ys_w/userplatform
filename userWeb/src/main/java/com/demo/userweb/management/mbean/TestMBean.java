package com.demo.userweb.management.mbean;

import com.demo.userweb.management.mbean.UserManager;
import com.demo.userweb.web.domain.User;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * 	getUser() 方法中的User无法识别
 */
public class TestMBean {
	
	public static void main(String[] args) throws Exception {
		
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		ObjectName objectName = new ObjectName("jmx.TestBean:type=user");	
		User user = new User();
		server.registerMBean(new UserManager(user), objectName);
		
		while(true) {
			Thread.sleep(2000);
			System.out.println(user);
		}

	}
}
