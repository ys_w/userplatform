package com.demo.userweb.management.dynamicbean;

import com.demo.userweb.management.mbean.UserManager;
import com.demo.userweb.web.domain.User;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class DemoDynamicBean {
    public static void main(String[] args) throws Exception {

        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        ObjectName objectName = new ObjectName("jmx.DemoDynamicBean:type=user");
        server.registerMBean(new DynamicSampleBean(), objectName);
        Thread.sleep(Long.MAX_VALUE);
    }
}
