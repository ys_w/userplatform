package com.demo.userweb.web;

import com.demo.mywebmvc.controller.PageController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.logging.Logger;

@Path("/page")
public class UserController implements PageController {

    private Logger logger = Logger.getLogger(UserController.class.getName());

    @GET
    @POST
    @Path("/toIndex")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.info( String.format("%s toIndex!", this.getClass().getName()) );
        return "/index.jsp";
    }

    @Path("/toHome")
    public String home(HttpServletRequest request, HttpServletResponse response){
        return "home.jsp";
    }

}
