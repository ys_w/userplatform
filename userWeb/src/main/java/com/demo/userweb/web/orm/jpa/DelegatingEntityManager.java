package com.demo.userweb.web.orm.jpa;

import com.demo.userweb.web.context.ComponentContext;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class DelegatingEntityManager implements EntityManager {

    private String persistenceUnitName;
    private String propertiesLocation;
    private EntityManager entityManager;

    @PostConstruct
    public void init(){
        System.out.println("Delegating Entity Manager init !");
        System.out.println("persistenceUnitName : "+persistenceUnitName);
        System.out.println("propertiesLocation : "+propertiesLocation);

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName, loadProperties());
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public Map loadProperties(){
        InputStream resourceAsStream = DelegatingEntityManager.class.getClassLoader().getResourceAsStream(propertiesLocation);

        try {
            Properties properties = new Properties();
            properties.load(resourceAsStream);

            System.out.println("JPA Properties : "+properties);

            ComponentContext componentContext = ComponentContext.getInstance();
            for (String name : properties.stringPropertyNames()){
                String propertyValue = properties.getProperty(name);
                if (propertyValue.startsWith("@")){
                    String componentName = propertyValue.substring(1);
                    Object component = componentContext.getComponent(componentName);
                    System.out.println("JPA Build Factory, found component : "+componentName+", "+component);
                    properties.put(name, component);
                }
            }
            return properties;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void persist(Object o) {
        this.entityManager.persist(o);
    }

    @Override
    public <T> T merge(T t) {
        return this.entityManager.merge(t);
    }

    @Override
    public void remove(Object o) {
        this.entityManager.remove(o);
    }

    @Override
    public <T> T find(Class<T> aClass, Object o) {
        return entityManager.find(aClass, o);
    }

    @Override
    public <T> T find(Class<T> aClass, Object o, Map<String, Object> map) {
        return this.entityManager.find(aClass, o, map);
    }

    @Override
    public <T> T find(Class<T> aClass, Object o, LockModeType lockModeType) {
        return this.entityManager.find(aClass, o, lockModeType);
    }

    @Override
    public <T> T find(Class<T> aClass, Object o, LockModeType lockModeType, Map<String, Object> map) {
        return this.entityManager.find(aClass, o, lockModeType, map);
    }

    @Override
    public <T> T getReference(Class<T> aClass, Object o) {
        return this.entityManager.getReference(aClass, o);
    }

    @Override
    public void flush() {
        this.entityManager.flush();
    }

    @Override
    public void setFlushMode(FlushModeType flushModeType) {
        this.entityManager.setFlushMode(flushModeType);
    }

    @Override
    public FlushModeType getFlushMode() {
        return this.entityManager.getFlushMode();
    }

    @Override
    public void lock(Object o, LockModeType lockModeType) {
        this.entityManager.lock(o, lockModeType);
    }

    @Override
    public void lock(Object o, LockModeType lockModeType, Map<String, Object> map) {
        this.entityManager.lock(o, lockModeType, map);
    }

    @Override
    public void refresh(Object o) {
        this.entityManager.refresh(o);
    }

    @Override
    public void refresh(Object o, Map<String, Object> map) {
        this.entityManager.refresh(o, map);
    }

    @Override
    public void refresh(Object o, LockModeType lockModeType) {
        this.entityManager.refresh(o, lockModeType);
    }

    @Override
    public void refresh(Object o, LockModeType lockModeType, Map<String, Object> map) {
        this.entityManager.refresh(o, lockModeType, map);
    }

    @Override
    public void clear() {
        this.entityManager.clear();
    }

    @Override
    public void detach(Object o) {
        this.entityManager.detach(o);
    }

    @Override
    public boolean contains(Object o) {
        return this.entityManager.contains(o);
    }

    @Override
    public LockModeType getLockMode(Object o) {
        return this.entityManager.getLockMode(o);
    }

    @Override
    public void setProperty(String s, Object o) {
        this.entityManager.setProperty(s, o);
    }

    @Override
    public Map<String, Object> getProperties() {
        return this.entityManager.getProperties();
    }

    @Override
    public Query createQuery(String s) {
        return this.entityManager.createQuery(s);
    }

    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
        return this.entityManager.createQuery(criteriaQuery);
    }

    @Override
    public Query createQuery(CriteriaUpdate criteriaUpdate) {
        return this.entityManager.createQuery(criteriaUpdate);
    }

    @Override
    public Query createQuery(CriteriaDelete criteriaDelete) {
        return this.entityManager.createQuery(criteriaDelete);
    }

    @Override
    public <T> TypedQuery<T> createQuery(String s, Class<T> aClass) {
        return this.entityManager.createQuery(s, aClass);
    }

    @Override
    public Query createNamedQuery(String s) {
        return this.entityManager.createNamedQuery(s);
    }

    @Override
    public <T> TypedQuery<T> createNamedQuery(String s, Class<T> aClass) {
        return this.entityManager.createNamedQuery(s, aClass);
    }

    @Override
    public Query createNativeQuery(String s) {
        return this.entityManager.createNativeQuery(s);
    }

    @Override
    public Query createNativeQuery(String s, Class aClass) {
        return this.entityManager.createNativeQuery(s, aClass);
    }

    @Override
    public Query createNativeQuery(String s, String s1) {
        return this.entityManager.createNativeQuery(s, s1);
    }

    @Override
    public StoredProcedureQuery createNamedStoredProcedureQuery(String s) {
        return this.entityManager.createNamedStoredProcedureQuery(s);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String s) {
        return this.entityManager.createStoredProcedureQuery(s);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String s, Class... classes) {
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String s, String... strings) {
        return null;
    }

    @Override
    public void joinTransaction() {
        this.entityManager.joinTransaction();
    }

    @Override
    public boolean isJoinedToTransaction() {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> aClass) {
        return null;
    }

    @Override
    public Object getDelegate() {
        return null;
    }

    @Override
    public void close() {
        this.entityManager.close();
    }

    @Override
    public boolean isOpen() {
        return this.entityManager.isOpen();
    }

    @Override
    public EntityTransaction getTransaction() {
        return this.entityManager.getTransaction();
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return this.entityManager.getEntityManagerFactory();
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return this.entityManager.getCriteriaBuilder();
    }

    @Override
    public Metamodel getMetamodel() {
        return this.entityManager.getMetamodel();
    }

    @Override
    public <T> EntityGraph<T> createEntityGraph(Class<T> aClass) {
        return null;
    }

    @Override
    public EntityGraph<?> createEntityGraph(String s) {
        return null;
    }

    @Override
    public EntityGraph<?> getEntityGraph(String s) {
        return null;
    }

    @Override
    public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> aClass) {
        return null;
    }

    public String getPersistenceUnitName() {
        return persistenceUnitName;
    }

    public void setPersistenceUnitName(String persistenceUnitName) {
        this.persistenceUnitName = persistenceUnitName;
    }

    public String getPropertiesLocation() {
        return propertiesLocation;
    }

    public void setPropertiesLocation(String propertiesLocation) {
        this.propertiesLocation = propertiesLocation;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
