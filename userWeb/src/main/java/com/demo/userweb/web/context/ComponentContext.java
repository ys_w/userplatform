package com.demo.userweb.web.context;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.naming.*;
import javax.servlet.ServletContext;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComponentContext {
    private static Context context;
    private static ServletContext servletContext;

    public static final String CONTEXT_NAME = ComponentContext.class.getName();
    private Logger logger = Logger.getLogger(this.getClass().getName());

    private Map<String, Object> componentsMap = new LinkedHashMap<>();
    private ClassLoader classLoader;

    public void init(ServletContext servletContext){

        this.servletContext = servletContext;
        servletContext.setAttribute(CONTEXT_NAME, this);
        classLoader = servletContext.getClassLoader();

        // 1. loop for all jndi component names
        // 2. look up those components and put into componentsMap
        // 3. init component, do dependency injection, and @postConstruct method invoke
        initEnvContext();
        instantiateComponents();
        initializeComponents();
    }

    private void initEnvContext(){
        try {
            Context initContext = new InitialContext();
            context = (Context)initContext.lookup("java:/comp/env");
        }catch (NamingException e){
            logger.log(Level.SEVERE, "ERROR!!! "+e.getMessage());
        }
    }

    private void instantiateComponents(){
        try {
            List<String> componentNames = listComponentNames("/");
            System.out.println(componentNames);
            for (String componentName : componentNames){
                componentsMap.put(componentName, context.lookup(componentName));
            }
        }catch (Exception e){
            logger.log(Level.SEVERE, e.getMessage());
        }
        System.out.println("componentMap : "+componentsMap);
    }

    private List<String> listComponentNames(String name){
        List<String> fullNameList = new ArrayList<>();

        try {
            NamingEnumeration<NameClassPair> namingEnumeration = context.list( name );

            while (namingEnumeration.hasMoreElements()){

                NameClassPair nameClassPair = namingEnumeration.next();

                String className = nameClassPair.getClassName();
                Class<?> clazz = classLoader.loadClass(className);

                if (Context.class.isAssignableFrom(clazz)){  // 目录节点
                    fullNameList.addAll( listComponentNames(nameClassPair.getName()) );
                } else {
                    fullNameList.add(name + "/" + nameClassPair.getName());
                }
            }
        }catch (Exception e){
            logger.log(Level.SEVERE, e.getMessage());
        }

        return fullNameList;
    }

    private void initializeComponents(){

        componentsMap.values().stream().forEach(component -> {

            Class<?> componentClass = component.getClass();

            // 1. inject dependency
            Field[] fields = componentClass.getDeclaredFields();

            for (Field field : fields){
                if (!Modifier.isStatic(field.getModifiers()) && field.isAnnotationPresent(Resource.class) ){
                    Resource resource = field.getAnnotation(Resource.class);
                    String resourceName = resource.name();
                    logger.info("inject dependency : "+resourceName);
                    try {
                        Object injectedObject = context.lookup(resourceName);
                        field.setAccessible(true);
                        field.set(component, injectedObject);

                    }catch (Exception e){
                        throw new RuntimeException(e);
                    }
                }
            }

            // 2. @postConstruct
            Method[] methods = componentClass.getDeclaredMethods();
            for (Method method : methods){
                if ( !Modifier.isStatic(method.getModifiers()) && method.isAnnotationPresent(PostConstruct.class) ){
                    try {
                        method.invoke(component);
                    }catch (Exception e){
                        throw new RuntimeException(e);
                    }
                }
            }
        });



    }

    public void destroy(){
        try {
            context.close();
        }catch (NamingException e){
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    public static ComponentContext getInstance(){
        return (ComponentContext) servletContext.getAttribute(CONTEXT_NAME);
    }

/*    public Object getComponent(String name){
        try {
            logger.info("GetComponent : " + name);
            return context.lookup( name );
        }catch (NamingException e){
            logger.log(Level.SEVERE, "ERROR!!! "+e.getMessage());
        }
        return null;
    }*/

    public Object getComponent(String name){
            logger.info("GetComponent : " + name);
            return componentsMap.get(name);
    }

    public Object lookup(String component) throws Exception {
        return context.lookup(component);
    }

}
