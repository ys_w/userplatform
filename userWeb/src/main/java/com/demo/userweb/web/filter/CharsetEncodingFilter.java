package com.demo.userweb.web.filter;

import javax.servlet.*;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Logger;

public class CharsetEncodingFilter implements Filter {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private ServletContext servletContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        servletContext = filterConfig.getServletContext();

        Enumeration<String> enumeration = filterConfig.getInitParameterNames();
        while (enumeration.hasMoreElements()){
            String initParamName = enumeration.nextElement();
            logger.info( String.format("filter init param %s is %s ", initParamName, filterConfig.getInitParameter(initParamName)) );
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        servletRequest.setCharacterEncoding("UTF-8");
        servletResponse.setCharacterEncoding("UTF-8");
        logger.info("filter set encoding UTF-8");

        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {
        logger.info("filter destory !");
    }
}
