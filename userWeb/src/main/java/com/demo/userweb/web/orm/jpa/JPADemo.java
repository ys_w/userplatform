package com.demo.userweb.web.orm.jpa;

import com.demo.userweb.web.domain.User;
import org.apache.derby.jdbc.EmbeddedDataSource;

import javax.persistence.*;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

public class JPADemo {

    private static Logger logger = Logger.getLogger(JPADemo.class.getName());

    @PersistenceContext
    private EntityManager entityManager;

    public static void main(String[] args) {

        logger.info("JPA Demo!");

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("emf", jpaProperties() );
        EntityManager entityManager = factory.createEntityManager();

        User user = new User();
        user.setName("JpaDemo");
        user.setEmail("email@email.com");
        user.setPassword("12345678910");
        user.setPhoneNumber("12345678");

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        //entityManager.createNativeQuery("select * from users").getResultList().forEach(Objects::toString);
        transaction.commit();

        logger.info( Objects.toString(entityManager.find(User.class, user.getId())) );
    }

    private static Map<String, Object> jpaProperties(){
        Map<String, Object> jpaProperties = new HashMap<>();
        jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.DerbyDialect");
        jpaProperties.put("hibernate.id.new_generator_mappings", false);
        jpaProperties.put("hibernate.connection.datasource", getDataSource() );

        return jpaProperties;
    }

    private static DataSource getDataSource(){

        EmbeddedDataSource dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("/db/user-platform");
        dataSource.setCreateDatabase("create");

        /*
        DataSource dataSource = (DataSource) ComponentContext.getInstance().getComponent("jdbc/UserPlatformDB");
        System.out.println("dataSource ： " + dataSource);
        */
        return dataSource;
    }

    public void demo(){
        User user = new User();
        user.setName("Demo");
        user.setEmail("email@email.com");
        user.setPassword("12345678910");
        user.setPhoneNumber("12345678");

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(user);
        transaction.commit();
    }

}
