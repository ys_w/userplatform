package com.demo.userweb.web.logging;


import java.io.InputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 *  Java.util.logging 的重要组件:
 *  LogRecord, Handler, Filter, Formatter
 */
public class UserWebLoggingConfig {

    public UserWebLoggingConfig() throws Exception{
        System.out.println("UserWebLoggingConfig!");
        Logger logger = Logger.getLogger("com.demo.userweb"); // 日志的属性是可以向下继承的,
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setEncoding("UTF-8");
        consoleHandler.setLevel(Level.ALL);
        logger.addHandler(consoleHandler);
    }

    public static void main(String[] args) throws Exception{

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("META-INF/logging.properties");
        LogManager logManager = LogManager.getLogManager();
        logManager.readConfiguration(inputStream);

        Logger logger = Logger.getLogger("com.demo.userweb");
        logger.setLevel(Level.ALL); // 设置 logger 的级别, 在配置文件中已经设置了handler的级别

        logger.info("Demo Log Info!");
        logger.warning("Demo Log Warning!");
        logger.severe("Demo Log SEVERE");
        logger.config("Demo Log CONFIG");
        logger.fine("Demo Log FINE");

    }
}
