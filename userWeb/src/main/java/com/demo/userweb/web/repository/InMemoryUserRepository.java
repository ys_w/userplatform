package com.demo.userweb.web.repository;

import com.demo.userweb.web.domain.User;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryUserRepository implements UserRepository {
    private Map<Long, User> repository = new ConcurrentHashMap<>();

    @Override
    public boolean save(User user) {
        repository.put(user.getId(), user);
        return true;
    }

    @Override
    public boolean deleteById(Long userId) {
        repository.remove(userId);
        return true;
    }

    @Override
    public boolean update(User user) {
        save(user);
        return true;
    }

    @Override
    public User getById(Long userId) {
        return repository.get(userId);
    }

    @Override
    public User getByNameAndPassword(String userName, String password) {
        return  repository
                .values()
                .stream()
                .filter( user -> user.getName().equals(userName) && user.getPassword().equals(password) )
                .findFirst()
                .get();
    }

    @Override
    public Collection<User> getAll() {
        return repository.values();
    }
}
