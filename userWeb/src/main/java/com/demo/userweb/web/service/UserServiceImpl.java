package com.demo.userweb.web.service;

import com.demo.userweb.web.domain.User;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class UserServiceImpl implements UserService{

    @Resource(name = "bean/entityManager")
    private EntityManager entityManager;

    @Override
    public void register(User user) {
        System.out.println("register user started !");
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();
        System.out.println("User registed : "+user.getId());
        System.out.println("register user succeed !");
    }

}
