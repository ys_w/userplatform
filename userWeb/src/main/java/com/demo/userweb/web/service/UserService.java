package com.demo.userweb.web.service;

import com.demo.userweb.web.domain.User;

public interface UserService {
    void register(User user);
}
