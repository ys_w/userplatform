package com.demo.userweb.web.listener;

import com.demo.configuration.apache.ApacheConfiguration;
import com.demo.configuration.microprofile.EclipseMicroProfileConfigurationDemo;
import com.demo.userweb.web.context.ComponentContext;
import com.demo.userweb.web.domain.User;
import com.demo.userweb.web.service.UserService;
import com.demo.userweb.web.sql.DBConnectionManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;

/**
 *  测试以JNDI 的方式模拟依赖注入,获取jdbc连接
 */
public class TestListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        DBConnectionManager dbConnectionManager = (DBConnectionManager) ComponentContext.getInstance().getComponent("bean/DBConnectionManager");
        System.out.println("dbConnectionManager : "+dbConnectionManager);

        Connection connection = null;
        try {
            connection = dbConnectionManager.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (connection != null){
            System.out.println("成功获取连接!" + connection);
        }

        // testUserService();
        //testApacheCommonsConfiguration();
        testMicroProfileConfiguration();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    private void testUserService(){
        User user = new User();
        user.setName("xiao");
        user.setEmail("email@email.com");
        user.setPhoneNumber("123456");
        user.setPassword("asldfkasdfje");

        UserService userService = (UserService) ComponentContext.getInstance().getComponent("bean/userService");
        System.out.println("userService : "+userService);
        userService.register(user);
    }

    private void testApacheCommonsConfiguration(){
        try {
            new ApacheConfiguration().testJndiMaxValue();
             new ApacheConfiguration().loadConfig();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void testMicroProfileConfiguration(){
        new EclipseMicroProfileConfigurationDemo().microProfileConfig();
    }

}
