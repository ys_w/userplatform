package com.demo.userweb.web.function;

@FunctionalInterface
public interface ThrowableFunction<T, R> {

    R apply(T t) throws Throwable;

    default R execute(T t){
        R result = null;
        try {
            result = apply(t);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        return result;
    }

    static <T, R> R execute(T t, ThrowableFunction<T, R> function){
        return function.execute(t);
    }

}

