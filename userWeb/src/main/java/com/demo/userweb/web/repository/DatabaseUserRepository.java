package com.demo.userweb.web.repository;

import com.demo.userweb.web.domain.User;
import com.demo.userweb.web.function.ThrowableFunction;
import com.demo.userweb.web.sql.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseUserRepository implements UserRepository{

    private Logger logger = Logger.getLogger(DatabaseUserRepository.class.getName());

    DBConnectionManager dbConnectionManager;

    public DatabaseUserRepository(){
    }

    public DatabaseUserRepository(DBConnectionManager dbConnectionManager){
        this.dbConnectionManager = dbConnectionManager;
    }

    public Connection getConnection(){
        return dbConnectionManager.getConnection();
    }

    @Override
    public boolean save(User user) {
        return false;
    }

    @Override
    public boolean deleteById(Long userId) {
        return false;
    }

    @Override
    public boolean update(User user) {
        return false;
    }

    @Override
    public User getById(Long userId) {
        return null;
    }

    // jdbc template 的思路
    @Override
    public User getByNameAndPassword(String userName, String password) {
        String sql = "SELECT id, name, password, email, phoneNumber  FROM users where username =? and password = ?";
        this.executeQuery(sql, resultSet -> {
            return mapResultSetToDomain(resultSet);
        }, throwable -> logger.log(Level.SEVERE, throwable.getMessage()));
        return null;
    }

    private List<User> mapResultSetToDomain(ResultSet resultSet){
        return null;
    }

    @Override
    public Collection<User> getAll() {
        return null;
    }

    protected <T> T executeQuery(String sql, ThrowableFunction<ResultSet, T> function, Consumer<Throwable> exceptionHandler, Object... args){
        Connection connection = getConnection();
        try {
             PreparedStatement ps = connection.prepareStatement(sql);
             for (int i=0; i<args.length; i++){
                ps.setObject(i, args[i]);
             }

             ResultSet resultSet = ps.executeQuery();
             return function.apply( resultSet );

        }catch (Throwable e){
            exceptionHandler.accept(e);
        }
        return null;
    }

}
