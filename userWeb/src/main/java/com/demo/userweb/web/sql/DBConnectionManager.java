package com.demo.userweb.web.sql;


import com.demo.userweb.web.domain.User;
import org.apache.commons.lang.StringUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnectionManager {

    public static Logger logger = Logger.getLogger(DBConnectionManager.class.getName());

    public static final String DROP_USERS_TABLE_DDL_SQL = "DROP TABLE users";
    public static final String CREATE_USERS_TABLE_DDL_SQL = "CREATE TABLE users(" +
                                                            "id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "+
                                                            "name VARCHAR(16) NOT NULL, "+
                                                            "password VARCHAR(64) NOT NULL, "+
                                                            "email VARCHAR(64) NOT NULL, "+
                                                            "phoneNumber VARCHAR(64) NOT NULL "+
                                                            ")";

    public static final String INSERT_USERS_DML_SQL = " INSERT INTO users(name, password, email, phoneNumber) VALUES "+
                                                      "('A', '*****', 'a@gmail.com', '1'), " +
                                                      "('B', '*****', 'b@gmail.com', '2'), "+
                                                      "('C', '*****', 'c@gmail.com', '3'), "+
                                                      "('D', '*****', 'd@gmail.com', '4'), "+
                                                      "('E', '*****', 'e@gmail.com', '5') ";

    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        try {
            Context initContext = new InitialContext();
            Context context = (Context) initContext.lookup("java:/comp/env");
            DataSource dataSource = (DataSource) context.lookup("jdbc/UserPlatformDB");
            if (dataSource != null){
                return dataSource.getConnection();
            }
        }catch (Exception e){
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

    public void releaseConnection(){
        if (connection!= null){
            try {
                connection.close();
            }catch (SQLException e){
                logger.log(Level.SEVERE, e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) throws Exception {

        String jdbcUrl = "jdbc:derby:/db/user-platform;create=true";
        Connection connection = DriverManager.getConnection(jdbcUrl);
        logger.info(String.format("connection is null : %s", String.valueOf(connection == null)));

        Statement stat = connection.createStatement();

        // state.execute() DDL, DML
        System.out.println(stat.execute(DROP_USERS_TABLE_DDL_SQL));
        System.out.println(stat.execute(CREATE_USERS_TABLE_DDL_SQL));
        System.out.println(stat.execute(INSERT_USERS_DML_SQL));

        ResultSet rs = stat.executeQuery("SELECT id, name , password, email, phoneNumber from users ");
        ResultSetMetaData metaData = rs.getMetaData();
        //System.out.println( String.format("MetaData Table Name : %s", metaData.getTableName(1)) );
        //System.out.println( String.format("MetaData Column Count : %s", metaData.getColumnCount()) );

        /*
        for (int i =1; i<= metaData.getColumnCount(); i++){
            System.out.println(String.format("Column label: %s, Column name: %s, Column type name: %s, Column class name: %s,",
                    metaData.getColumnLabel(i), metaData.getColumnName(i), metaData.getColumnTypeName(i), metaData.getColumnClassName(i) ) );
        }
        */

        List userList = mapResultToDomain(User.class, rs);
        System.out.println("userList : "+userList);
    }

    private static List<Object> mapResultToDomain(Class clazz, ResultSet rs) throws Exception{
        BeanInfo beanInfo = Introspector.getBeanInfo(clazz, Object.class);
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();

        List<Object> list = new ArrayList<>();

        while (rs.next()){
            Object obj = clazz.newInstance();

            for (PropertyDescriptor propertyDescriptor : propertyDescriptors){
                String fieldName = propertyDescriptor.getName();
                Class  fieldType = propertyDescriptor.getPropertyType();

                System.out.println(String.format("fieldName : %s, fieldType : %s", fieldName, fieldType.getName()));

                Method writeMethod = propertyDescriptor.getWriteMethod();

                String rsMethodName = "get"+StringUtils.capitalize(fieldType.getSimpleName());
                System.out.println("rsMethodName : " + rsMethodName);
                Method rsGetter = rs.getClass().getMethod(rsMethodName, String.class);
                Object value = rsGetter.invoke(rs, fieldName);
                //Object value = fieldType.cast( rs.getString(fieldName) );

                writeMethod.invoke(obj, value);
            }
            list.add(obj);
        }
        return list;
    }

}
