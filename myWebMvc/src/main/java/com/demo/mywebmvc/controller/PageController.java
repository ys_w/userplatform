package com.demo.mywebmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface PageController extends Controller{
    public String execute(HttpServletRequest request, HttpServletResponse response);
}
