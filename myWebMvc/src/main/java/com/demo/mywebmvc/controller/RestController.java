package com.demo.mywebmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RestController extends Controller{
    public Object execute(HttpServletRequest request, HttpServletResponse response);
}
