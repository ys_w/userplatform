package com.demo.mywebmvc.servlet;

import com.demo.mywebmvc.MethodInfo;
import com.demo.mywebmvc.controller.Controller;
import com.demo.mywebmvc.controller.PageController;
import com.demo.mywebmvc.controller.RestController;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Logger;

public class FrontServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private Map<String, MethodInfo> methodsInfo = new HashMap<>();
    private Map<String, Controller> controllerMap = new HashMap<>();

    @Override
    public void init() throws ServletException {
        logger.info("Init: collect method info! ");
        this.collectMethodInfo();
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestUri = request.getRequestURI();
        String contextPath = request.getServletContext().getContextPath();
        String requestPath = StringUtils.substringAfter(requestUri, contextPath);

        logger.info( String.format("ContextPath : %s, RequestPath : %s", contextPath, requestPath) );

        MethodInfo methodInfo = methodsInfo.get(requestPath);
        Method method = methodInfo.getMethod();
        String methodName = method.getName();

        Controller controller  = controllerMap.get(requestPath);
        PageController pageController = PageController.class.cast(controller);

        String viewPath = StringUtils.EMPTY;
        try {
            if (!methodName.equalsIgnoreCase("execute")){
                viewPath = (String) method.invoke(pageController, request, response);
            }else {
                viewPath = pageController.execute(request, response);
            }
        }catch (Exception e){
            logger.severe(e.getMessage());
        }

        if (!viewPath.startsWith("/")){
            viewPath = "/" + viewPath;
        }
        logger.info("viewPath : "+viewPath);

        /**
         *  如下两种不同方式获取的 RequestDispatcher 有什么异同
         *  request.getRequestDispatcher()
         *  request.getServletContext().getRequestDispatcher()
         */
        request.getServletContext().getRequestDispatcher(viewPath).forward(request, response);
    }

    private void collectMethodInfo(){
        Iterator iterator = ServiceLoader.load(Controller.class).iterator();
        while (iterator.hasNext()){
            Controller controller = (Controller) iterator.next();
            if (controller instanceof PageController){
                Path path = controller.getClass().getAnnotation(Path.class);
                String classLevelPath = (path != null) ? path.value() : "";
                logger.info( String.format("class level path : %s", classLevelPath) );

                Method[] methods = controller.getClass().getDeclaredMethods();
                createRequestMapping(classLevelPath, methods, controller);
            }else{
                logger.info("RestController" );
            }
        }
    }

    private void createRequestMapping(String classLevelPath, Method[] methods, Controller controller){
        logger.info("start create request mapping!");
        if (methods == null){
            logger.info("No method defined !");
            return;
        }

        for (Method method : methods){
            Path path = method.getAnnotation(Path.class);
            if (path == null) continue;
            String methodLevelPath = path.value();

            if (!StringUtils.startsWith(methodLevelPath, "/")){
                methodLevelPath = "/" + methodLevelPath;
            }

            Set<String> set = collectHttpMethod(method);
            logger.info( String.format("method : %s(),  path : %s", method.getName(), methodLevelPath) );

            String requestPath = classLevelPath + methodLevelPath;
            logger.info( String.format("method %s(),  requestPath : %s", method.getName(), requestPath) );

            methodsInfo.put(requestPath, new MethodInfo(requestPath, set, method));
            controllerMap.put(requestPath, controller);
        }

    }

    private Set collectHttpMethod(Method method){
        Set<String> methods = new HashSet<>();
        List<String> httpMethodsList = Arrays.asList(HttpMethod.GET, HttpMethod.POST, HttpMethod.HEAD, HttpMethod.PUT, HttpMethod.DELETE, HttpMethod.OPTIONS);

        Annotation[] annotations = method.getAnnotations();
        if (annotations == null){
            methods.addAll(httpMethodsList);
            return methods;
        }

        for (Annotation annotation : annotations){
            HttpMethod httpMethod = annotation.annotationType().getAnnotation(HttpMethod.class);
            if (httpMethod != null){
                methods.add(httpMethod.value());
            }
        }
        if (methods.isEmpty()){
            methods.addAll(methods);
        }
        return methods;
    }

}
