package com.demo.mywebmvc;

import java.lang.reflect.Method;
import java.util.Set;

public class MethodInfo {

    private String path;
    private Set<String> httpMethods;
    private Method method;

    public MethodInfo(){
    }

    public MethodInfo(String path, Set<String> httpMethods, Method method) {
        this.path = path;
        this.httpMethods = httpMethods;
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }
}
