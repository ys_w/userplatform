package com.demo.configuration.microprofile.converter;

import com.demo.configuration.microprofile.apiImpl.converter.ByteConverter;
import com.demo.configuration.microprofile.apiImpl.converter.Converters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConvertersTest {

    private Converters converters;

    @Before
    public void init(){
        converters = new Converters();
    }

    @Test
    public void testResolveConvertedType(){
        Assert.assertEquals(Byte.class, converters.resolveClassType(new ByteConverter()));
    }

}
