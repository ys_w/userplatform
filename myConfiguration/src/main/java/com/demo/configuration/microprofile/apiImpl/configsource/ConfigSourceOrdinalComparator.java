package com.demo.configuration.microprofile.apiImpl.configsource;

import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.Comparator;

public class ConfigSourceOrdinalComparator implements Comparator<ConfigSource> {

    public static final Comparator<ConfigSource> INSTANCE = new ConfigSourceOrdinalComparator();

    private ConfigSourceOrdinalComparator(){
    }

    @Override
    public int compare(ConfigSource o1, ConfigSource o2) {
       return Integer.compare(o2.getOrdinal(), o1.getOrdinal());
    }

}
