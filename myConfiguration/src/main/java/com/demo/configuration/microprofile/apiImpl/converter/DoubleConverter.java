package com.demo.configuration.microprofile.apiImpl.converter;

import org.eclipse.microprofile.config.spi.Converter;

public class DoubleConverter extends AbstractConverter<Double> {

    @Override
    protected Double doConvert(String s) {
        return Double.parseDouble(s);
    }
}
