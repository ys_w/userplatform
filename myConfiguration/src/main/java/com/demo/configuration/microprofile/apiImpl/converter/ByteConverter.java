package com.demo.configuration.microprofile.apiImpl.converter;

public class ByteConverter extends AbstractConverter<Byte>{
    @Override
    protected Byte doConvert(String s) {
        return Byte.parseByte(s);
    }
}
