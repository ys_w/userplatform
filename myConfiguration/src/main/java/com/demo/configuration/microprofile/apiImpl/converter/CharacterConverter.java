package com.demo.configuration.microprofile.apiImpl.converter;

public class CharacterConverter extends AbstractConverter<Character>{

    @Override
    protected Character doConvert(String s) {
        return Character.valueOf( s.charAt(0) );
    }
}
