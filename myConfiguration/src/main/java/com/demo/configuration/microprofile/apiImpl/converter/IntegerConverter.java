package com.demo.configuration.microprofile.apiImpl.converter;

public class IntegerConverter extends AbstractConverter<Integer> {

    @Override
    protected Integer doConvert(String s) {
        return Integer.parseInt(s);
    }
}
