package com.demo.configuration.microprofile.apiImpl;

import com.demo.configuration.microprofile.apiImpl.configsource.ConfigSources;
import com.demo.configuration.microprofile.apiImpl.converter.Converters;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigValue;
import org.eclipse.microprofile.config.spi.ConfigSource;
import org.eclipse.microprofile.config.spi.Converter;

import java.util.List;
import java.util.Optional;

public class DefaultConfig implements Config {

    private final ConfigSources configSources;
    private final Converters converters;

    DefaultConfig(ConfigSources configSources, Converters converters) {
        this.configSources = configSources;
        this.converters = converters;
    }

    @Override
    public <T> T getValue(String propertyName, Class<T> aClass) {
        ConfigValue configValue = getConfigValue( propertyName );
        if (configValue == null) return null;
        Converter<T> converter = doGetConverter(aClass);
        return converter == null ? null : converter.convert(configValue.getValue());
    }

    private String getPropertyValue(String propertyName){
        String propertyValue = null;
        for (ConfigSource configSource : configSources){
            propertyValue = configSource.getValue(propertyName);
            if (propertyValue != null){
                break;
            }
        }
        return propertyValue;
    }

    protected <T> Converter<T> doGetConverter(Class<T> forType){
        List<Converter> converters = this.converters.getConverters(forType);
        return converters.isEmpty()? null : converters.get(0);
    }

    @Override
    public ConfigValue getConfigValue(String propertyName) {
        String propertyValue = null;
        ConfigSource targetConfigSource = null;
        for (ConfigSource configSource : configSources){
            propertyValue = configSource.getValue(propertyName);
            if (propertyValue != null){
                targetConfigSource = configSource;
                break;
            }
        }

        if (propertyValue == null) return null;

        return new DefaultConfigValue(propertyName, propertyValue, propertyValue,
                targetConfigSource.getName(),
                targetConfigSource.getOrdinal());
    }

    @Override
    public <T> List<T> getValues(String propertyName, Class<T> propertyType) {
        return null;
    }

    @Override
    public <T> Optional<T> getOptionalValue(String s, Class<T> aClass) {
        return Optional.empty();
    }

    @Override
    public <T> Optional<List<T>> getOptionalValues(String propertyName, Class<T> propertyType) {
        return Optional.empty();
    }

    @Override
    public Iterable<String> getPropertyNames() {
        return null;
    }

    @Override
    public Iterable<ConfigSource> getConfigSources() {
        return null;
    }

    @Override
    public <T> Optional<Converter<T>> getConverter(Class<T> aClass) {
        return Optional.empty();
    }

    @Override
    public <T> T unwrap(Class<T> aClass) {
        return null;
    }
}
