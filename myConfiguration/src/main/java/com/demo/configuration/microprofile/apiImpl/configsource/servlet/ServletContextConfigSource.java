package com.demo.configuration.microprofile.apiImpl.configsource.servlet;

import com.demo.configuration.microprofile.apiImpl.configsource.MapBasedConfigSource;

import javax.servlet.ServletContext;
import java.util.Enumeration;
import java.util.Map;

public class ServletContextConfigSource extends MapBasedConfigSource {

    private final ServletContext servletContext;

    public ServletContextConfigSource(ServletContext servletContext){
        super(500, "Servlet Context Init Parameters");
        this.servletContext = servletContext;
    }

    @Override
    protected void prepareData(Map<String, String> configData) throws Throwable {
        Enumeration<String> initParameterNames = servletContext.getInitParameterNames();
        while (initParameterNames.hasMoreElements()){
            String parameterName = initParameterNames.nextElement();
            String value = servletContext.getInitParameter(parameterName);
            configData.put(parameterName, value);
        }
    }
}
