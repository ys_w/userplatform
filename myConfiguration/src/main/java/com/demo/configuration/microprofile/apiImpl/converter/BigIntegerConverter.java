package com.demo.configuration.microprofile.apiImpl.converter;

import java.math.BigInteger;

public class BigIntegerConverter extends AbstractConverter<BigInteger>{

    @Override
    protected BigInteger doConvert(String s) {
        return new BigInteger(s);
    }
}
