package com.demo.configuration.microprofile.apiImpl.converter;

public class LongConverter extends AbstractConverter<Long>{

    @Override
    protected Long doConvert(String s) {
        return Long.parseLong(s);
    }
}
