package com.demo.configuration.microprofile.apiImpl.converter;

public class ShortConverter extends AbstractConverter<Short>{

    @Override
    protected Short doConvert(String s) {
        return Short.parseShort(s);
    }

}
