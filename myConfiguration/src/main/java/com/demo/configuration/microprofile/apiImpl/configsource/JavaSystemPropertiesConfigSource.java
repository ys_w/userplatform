package com.demo.configuration.microprofile.apiImpl.configsource;

import java.util.Map;

public class JavaSystemPropertiesConfigSource extends MapBasedConfigSource {

    public JavaSystemPropertiesConfigSource() {
        super(400, "Java System Properties Config Source");
    }

    @Override
    protected void prepareData(Map configData) throws Throwable {
        configData.putAll(System.getProperties());
    }

}
