package com.demo.configuration.microprofile.apiImpl.configsource;

import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class MapBasedConfigSource implements ConfigSource {

    private final int ordinal;
    private final String name;
    private final Map<String, String> sources;

    public MapBasedConfigSource(int ordinal, String name){
        this.ordinal = ordinal;
        this.name = name;
        this.sources = getProperties();
    }

    @Override
    public Map<String, String> getProperties() {
        Map<String, String> configData = new HashMap<>();
        try {
            prepareData(configData);
        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
        return Collections.unmodifiableMap(configData);
    }

    protected abstract void prepareData(Map<String, String> configData) throws Throwable;

    @Override
    public Set<String> getPropertyNames() {
        return sources.keySet();
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public String getValue(String s) {
        return sources.get(s);
    }

    @Override
    public String getName() {
        return name;
    }
}
