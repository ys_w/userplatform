package com.demo.configuration.microprofile.apiImpl.configsource;

import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.*;

/**
 * 实现 迭代器模式 ( iterator patten )
 */
public class ConfigSources implements Iterable<ConfigSource>{

    private ClassLoader classLoader;
    private boolean addedDefaultConfigSources = false;
    private boolean addedDiscoveredConfigSources = false;

    private List<ConfigSource> configSources = new LinkedList<>();

    public ConfigSources(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public void addDefaultSources(){
        if (addedDefaultConfigSources){
            return;
        }

        addConfigSources(
                JavaSystemPropertiesConfigSource.class,
                OSEnvironmentVariableConfigSource.class,
                DefaultSourceConfigSource.class);

        addedDefaultConfigSources = true;
    }

    public void addDiscoveredSources(){
        if (addedDiscoveredConfigSources){
            return;
        }
        Iterable<ConfigSource> discoveredSources = ServiceLoader.load(ConfigSource.class);
        addConfigSources(discoveredSources);
        addedDiscoveredConfigSources = true;
    }

    public void addConfigSources(Class<? extends ConfigSource>... configSourceClasses){
        List<ConfigSource> temp = new LinkedList<>();
        try {
            for (Class<? extends ConfigSource> clazz : configSourceClasses){
                 temp.add( clazz.newInstance() );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        addConfigSources(temp);
    }

    public void addConfigSources(Iterable<ConfigSource> iterableConfigSources){
        for (ConfigSource source : iterableConfigSources){
            configSources.add(source);
        }
        Collections.sort(configSources, ConfigSourceOrdinalComparator.INSTANCE);
    }

    @Override
    public Iterator<ConfigSource> iterator() {
        return configSources.iterator();
    }

    public boolean isAddedDefaultConfigSources() {
        return addedDefaultConfigSources;
    }

    public boolean isAddedDiscoveredConfigSources() {
        return addedDiscoveredConfigSources;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader){
        this.classLoader = classLoader;
    }

}
