package com.demo.configuration.microprofile.apiImpl.configsource.servlet;

import com.demo.configuration.microprofile.apiImpl.DefaultConfigProviderResolver;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigBuilder;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ServletContextConfigInitializer implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        ServletContextConfigSource servletContextConfigSource = new ServletContextConfigSource(servletContext);
        ClassLoader classLoader = servletContext.getClassLoader();
        ConfigBuilder builder = DefaultConfigProviderResolver.instance().getBuilder();
        builder.addDefaultSources().addDiscoveredSources().withSources(servletContextConfigSource);
        Config config = builder.build();

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
