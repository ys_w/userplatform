package com.demo.configuration.microprofile.apiImpl;

import com.demo.configuration.microprofile.apiImpl.configsource.ConfigSources;
import com.demo.configuration.microprofile.apiImpl.converter.Converters;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigBuilder;
import org.eclipse.microprofile.config.spi.ConfigSource;
import org.eclipse.microprofile.config.spi.Converter;

import java.util.Arrays;

public class DefaultConfigBuilder implements ConfigBuilder {

    private ConfigSources configSources;
    private Converters converters;

    public DefaultConfigBuilder(ConfigSources configSources, Converters converters){
        this.configSources = configSources;
        this.converters = converters;
    }

    public DefaultConfigBuilder(ClassLoader classLoader){
        configSources = new ConfigSources(classLoader);
        converters = new Converters();
    }


    @Override
    public ConfigBuilder addDefaultSources() {
        configSources.addDefaultSources();
        return this;
    }

    @Override
    public ConfigBuilder addDiscoveredSources() {
        configSources.addDiscoveredSources();
        return this;
    }

    @Override
    public ConfigBuilder addDiscoveredConverters() {
        converters.addDiscoveredConverter();
        return this;
    }

    @Override
    public ConfigBuilder forClassLoader(ClassLoader classLoader) {
        configSources.setClassLoader(classLoader);
        converters.setClassLoader(classLoader);
        return this;
    }

    @Override
    public ConfigBuilder withSources(ConfigSource... configSources) {
        this.configSources.addConfigSources(Arrays.asList(configSources));
        return this;
    }

    @Override
    public ConfigBuilder withConverters(Converter<?>... converters) {
        this.converters.addConverters(converters);
        return this;
    }

    @Override
    public <T> ConfigBuilder withConverter(Class<T> aClass, int priority, Converter<T> converter) {
        this.converters.addConverter(converter, priority, aClass);
        return this;
    }

    @Override
    public Config build() {
        DefaultConfig defaultConfig = new DefaultConfig(this.configSources, this.converters);
        return defaultConfig;
    }

}
