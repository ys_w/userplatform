package com.demo.configuration.microprofile.apiImpl.converter;

import java.math.BigDecimal;

public class BigDecimalConverter extends AbstractConverter<BigDecimal>{

    @Override
    protected BigDecimal doConvert(String s) {
        return new BigDecimal(s);
    }

}
