package com.demo.configuration.microprofile.apiImpl.test;

import org.eclipse.microprofile.config.spi.Converter;

import java.util.PriorityQueue;

public class DemoPriorityQueue {
    public void priorityQueue(){
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.add(100);
        queue.add(200);
        System.out.println(queue.peek());

        PriorityQueue<Item> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Item("item1", 100));
        priorityQueue.add(new Item("item3", 300));
        priorityQueue.add(new Item("item2", 200));
        System.out.println(priorityQueue.peek().getName());

        for (Item item : priorityQueue){
            System.out.println(item);
        }

    }

    public static void main(String[] args) {
        new DemoPriorityQueue().priorityQueue();
    }

    class Item implements Comparable<Item>{
        private String name;
        private Integer priority;

        public Item(String name, Integer priority){
            this.name = name;
            this.priority = priority;
        }

        @Override
        public int compareTo(Item item) {
            return item.getPriority().compareTo(this.getPriority());
        }

        public String getName() {
            return name;
        }

        public Integer getPriority() {
            return priority;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "name='" + name + '\'' +
                    ", priority=" + priority +
                    '}';
        }
    }

}
