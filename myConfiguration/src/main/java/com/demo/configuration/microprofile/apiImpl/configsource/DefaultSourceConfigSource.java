package com.demo.configuration.microprofile.apiImpl.configsource;

import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.Map;
import java.util.Properties;

public class DefaultSourceConfigSource extends MapBasedConfigSource {

    private final String configFileLocation = "META-INF/microprofile-config.properties";

    public DefaultSourceConfigSource(){
        super(100, "DefaultSourceConfigSource");
    }

    @Override
    protected void prepareData(Map configData) throws Throwable {
        Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream(configFileLocation));
        configData.putAll(properties);
    }

}
