package com.demo.configuration.microprofile.apiImpl.converter;

import org.eclipse.microprofile.config.spi.Converter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

public class Converters<T> implements Iterable<Converter>{
    private final int DEFAULT_PRIORITY = 100;
    private ClassLoader classLoader;
    private boolean addedDiscoveredConverters = false;
    private Map<Class, PriorityQueue<PrioritizedConverter>> typedConverters = new HashMap<>();

    public Converters(){
        this(Thread.currentThread().getContextClassLoader());
    }

    public Converters(ClassLoader classLoader){
        this.classLoader = classLoader;
    }

    public void addDiscoveredConverter(){
        int priorityForDiscoveredConverter = 500;
        if (addedDiscoveredConverters){
            return;
        }
        //addConverters(ServiceLoader.load(Converter.class));
        Iterable<Converter> discoveredConverters = ServiceLoader.load(Converter.class);
        for (Converter converter : discoveredConverters){
            addConverter(converter, priorityForDiscoveredConverter);
        }
        addedDiscoveredConverters = true;
    }

    public void addConverters(Converter... converters){
        addConverters(Arrays.asList(converters));
    }

    public void addConverters(Iterable<Converter> converters){
        converters.forEach(this::addConverter);
    }

    public void addConverter(Converter converter){
        addConverter(converter, DEFAULT_PRIORITY);
    }

    public void addConverter(Converter converter, int priority){
        Class clazz = resolveClassType(converter);
        addConverter(converter, priority, clazz);
    }

    public void addConverter(Converter converter, int priority, Class clazz){
        PriorityQueue<PrioritizedConverter> targetQueue = typedConverters.get(clazz);
        if (targetQueue == null){
            targetQueue = new PriorityQueue<>();
        }
        targetQueue.add(new PrioritizedConverter(converter, priority));
        typedConverters.put(clazz, targetQueue);
    }

    public Class resolveClassType(Converter converter){
        Class clazz = null;
        Type type = converter.getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType){
            ParameterizedType pType = (ParameterizedType) type;
            clazz = (Class) pType.getActualTypeArguments()[0];
        }

        if (clazz == null){
            Type[] types = converter.getClass().getGenericInterfaces();
            for (Type t : types){
                if (t instanceof ParameterizedType){
                    ParameterizedType pt = (ParameterizedType) t;
                    clazz = (Class) pt.getActualTypeArguments()[0];
                }
            }
        }

        return clazz;
    }

    public List<Converter> getConverters(Class convertedType){
        PriorityQueue<PrioritizedConverter> prioritizedConverters = typedConverters.get(convertedType);
        if((prioritizedConverters == null) || (prioritizedConverters.isEmpty())){
            return Collections.emptyList();
        }

        List<Converter> converters = new LinkedList<>();
        for (PrioritizedConverter prioritizedConverter : prioritizedConverters){
            converters.add(prioritizedConverter.getConverter());
        }
        return converters; // converters[0] 一定是priority最高的那个converter
    }

    // 返回所有的 Converter
    @Override
    public Iterator<Converter> iterator() {
        List<Converter> allConverters = new LinkedList<>();
        Collection<PriorityQueue<PrioritizedConverter>> values = typedConverters.values();
        for (PriorityQueue<PrioritizedConverter> priorityQueue : values){
            for (PrioritizedConverter converter : priorityQueue){
                allConverters.add(converter);
            }
        }
        return allConverters.iterator();
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
