package com.demo.configuration.microprofile.apiImpl.configsource;

import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.Map;

public class OSEnvironmentVariableConfigSource extends MapBasedConfigSource{

    public OSEnvironmentVariableConfigSource() {
        super(300, "OSEnvVarConfigSource");
    }

    @Override
    protected void prepareData(Map<String, String> configData) throws Throwable {
        configData.putAll(System.getenv());
    }

}
