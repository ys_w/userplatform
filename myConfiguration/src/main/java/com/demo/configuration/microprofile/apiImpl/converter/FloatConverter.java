package com.demo.configuration.microprofile.apiImpl.converter;

import org.eclipse.microprofile.config.spi.Converter;

public class FloatConverter extends AbstractConverter<Float> {

    @Override
    protected Float doConvert(String s) {
        return Float.parseFloat(s);
    }
}
