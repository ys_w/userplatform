package com.demo.configuration.microprofile.apiImpl.converter;

public class StringConverter extends AbstractConverter<String>{

    @Override
    protected String doConvert(String s) {
        return s;
    }

}
