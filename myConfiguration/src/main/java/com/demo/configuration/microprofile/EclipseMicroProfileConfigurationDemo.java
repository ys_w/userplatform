package com.demo.configuration.microprofile;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.spi.ConfigBuilder;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;

import javax.sql.DataSource;

public class EclipseMicroProfileConfigurationDemo {


    public void microProfileConfig(){

        //defaultConfig();
        //demoConfigResolver();
        //demoAPI();
    }

    /**
     * java -Dartifact=demo -jar userWeb-1.0-SNAPSHOT-war-exec.jar
     *
     * 使用geronimo-config-impl 作为 MircoProfile Config 的实现演示如何使用MircoProfile Config
     */
    private void defaultConfig(){
        Config config = ConfigProvider.getConfig();
        String val = config.getValue("artifact", String.class);
        System.out.println("microProfileConfig val : "+val);
    }

    /**
     *  使用geronimo-config-impl 作为MircoProfile Config 的实现演示如何使用MircoProfile Config
     */
    private void demoConfigResolver(){
        ConfigProviderResolver resolver = ConfigProviderResolver.instance();
        ConfigBuilder builder = resolver.getBuilder();
        Config config = builder.addDefaultSources() // 添加默认configsource
                                .withSources(new MyJNDIConfigSource()) // 添加自定义 configsource
                                .withConverters(new MyConverter()) // 添加自定义转换器
                                .build();
        DataSource val = config.getValue("jndiDataSource", DataSource.class);
        System.out.println("jdbc/UserPlatformDB val : "+val);
    }


}
