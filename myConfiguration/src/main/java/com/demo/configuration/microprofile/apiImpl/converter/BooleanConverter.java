package com.demo.configuration.microprofile.apiImpl.converter;

public class BooleanConverter extends AbstractConverter<Boolean>{

    @Override
    protected Boolean doConvert(String s) {
        return Boolean.parseBoolean(s);
    }

}
