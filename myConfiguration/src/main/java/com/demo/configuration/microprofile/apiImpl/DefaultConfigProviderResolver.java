package com.demo.configuration.microprofile.apiImpl;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigBuilder;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class DefaultConfigProviderResolver extends ConfigProviderResolver {

    private ConcurrentMap<ClassLoader, Config> configRepository = new ConcurrentHashMap<>();

    @Override
    public Config getConfig() {
        return null;
    }

    @Override
    public Config getConfig(ClassLoader classLoader) {
        return null;
    }

    @Override
    public ConfigBuilder getBuilder() {
        return newConfigBuilder(null);
    }

    protected DefaultConfigBuilder newConfigBuilder(ClassLoader classLoader){
        return new DefaultConfigBuilder(resolveClassLoader(classLoader));
    }

    @Override
    public void registerConfig(Config config, ClassLoader classLoader) {
    }

    @Override
    public void releaseConfig(Config config) {
    }

    private ClassLoader resolveClassLoader(ClassLoader classLoader){
        return classLoader == null? this.getClass().getClassLoader() : classLoader;
    }

}
