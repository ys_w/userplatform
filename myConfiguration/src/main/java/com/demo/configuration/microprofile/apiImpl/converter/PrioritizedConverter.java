package com.demo.configuration.microprofile.apiImpl.converter;

import org.eclipse.microprofile.config.spi.Converter;

public class PrioritizedConverter<T> implements Converter<T>, Comparable<PrioritizedConverter<T>>  {

    private final Converter<T> converter;
    private final int priority;

    public PrioritizedConverter(Converter<T> converter, int priority){
        this.converter = converter;
        this.priority = priority;
    }

    @Override
    public T convert(String s) throws IllegalArgumentException, NullPointerException {
        return converter.convert(s);
    }

    @Override
    public int compareTo(PrioritizedConverter<T> o) {
        return Integer.compare(o.getPriority(), this.getPriority());
    }

    public int getPriority() {
        return priority;
    }

    public Converter<T> getConverter() {
        return converter;
    }

}
