package com.demo.configuration.microprofile;

//import com.demo.userweb.web.context.ComponentContext;
import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MyJNDIConfigSource implements ConfigSource {

    @Override
    public Map<String, String> getProperties() {
        return new HashMap<>();
    }

    @Override
    public Set<String> getPropertyNames() {
        return null;
    }

    @Override
    public int getOrdinal() {
        return 500;
    }

    @Override
    public String getValue(String s) {
        try {
            //return (String) ComponentContext.getInstance().lookup(s);
            return null;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public String getName() {
        return "MyJNDIConfigSource";
    }
}
