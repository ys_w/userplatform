package com.demo.configuration.apache;

//import com.demo.userweb.web.context.ComponentContext;
import org.apache.commons.configuration.*;

import javax.naming.Context;
import javax.naming.InitialContext;

public class ApacheConfiguration {

    /**
     *   以统一的 Configuration 接口取处理所有不同的配置源(这里包括properties和jndi), 这就是为什么要使用 Apache Commons Configuration
     */
    public Configuration loadConfig() throws Exception {
        CompositeConfiguration compositeConfiguration = new CompositeConfiguration();

        // properties configuration
        Configuration propertiesConfig = new PropertiesConfiguration("META-INF/logging.properties");
        System.out.println(propertiesConfig.getString("java.util.logging.FileHandler.formatter"));
        System.out.println(propertiesConfig.getString("handlers"));

        // jndi configuration
        Context context = (Context) new InitialContext().lookup("java:/comp/env");
        Configuration jndiConfig = new JNDIConfiguration(context);
        System.out.println("jndi config maxValue : "+jndiConfig.containsKey("maxValue"));
        System.out.println("jndi config bean/userService : "+jndiConfig.containsKey("bean/userService"));
        System.out.println("jndi config bean/demo : "+jndiConfig.containsKey("bean/demo"));

        compositeConfiguration.addConfiguration( propertiesConfig );
        compositeConfiguration.addConfiguration(jndiConfig);
        return compositeConfiguration;
    }

    public void testJndiMaxValue() throws Exception{
        //Integer maxValue = (Integer) ComponentContext.getInstance().lookup("maxValue");
        //System.out.println("maxValue : " + maxValue);
    }

}
